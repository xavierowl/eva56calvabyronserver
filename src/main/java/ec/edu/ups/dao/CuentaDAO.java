package ec.edu.ups.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ec.edu.ups.modelos.Cliente;
import ec.edu.ups.modelos.Cuenta;

@Stateless
public class CuentaDAO {
	@PersistenceContext
	EntityManager em;
	
	public boolean crearCuenta(Cuenta cuenta) throws Exception {
		try {
			em.persist(cuenta);
			return true;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	public List<Cuenta> listarCuentas() throws Exception{
		try {
			return em.createQuery("SELECT c FROM Cuenta c", Cuenta.class).getResultList();
		} catch (Exception e) {
			throw new Exception(e);
			}
	}
}
