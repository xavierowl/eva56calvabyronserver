package ec.edu.ups.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import ec.edu.ups.modelos.Cliente;

@Stateless
public class ClienteDAO {
	@PersistenceContext
	EntityManager em;
	
	public boolean crearCliente(Cliente cliente) throws Exception {
		try {
			em.persist(cliente);
			return true;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	public List<Cliente> listarClientes() throws Exception{
		try {
			return em.createQuery("SELECT c FROM Cliente c", Cliente.class).getResultList();
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	public Cliente buscarCliente(String cedula) throws Exception {
		try {
			return em.createQuery("SELECT c FROM Cliente c WHERE c.cli_cedula = '"+cedula+"'", Cliente.class).getSingleResult();
		} catch (Exception e) {
			throw new Exception("Error buscando el cliente. ("+e.getMessage()+")");
		}
	}
}
