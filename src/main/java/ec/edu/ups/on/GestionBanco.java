package ec.edu.ups.on;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.dao.ClienteDAO;
import ec.edu.ups.dao.CuentaDAO;
import ec.edu.ups.modelos.Cliente;
import ec.edu.ups.modelos.Cuenta;

@Stateless
public class GestionBanco implements GestionBancoLocal, GestionBancoRemote{
	@Inject
	private ClienteDAO cdao;
	
	@Inject
	private CuentaDAO cudao;

	public boolean crearCliente(Cliente cliente) throws Exception {
		return cdao.crearCliente(cliente);
	}
	
	public boolean crearCuenta(Cuenta cuenta) throws Exception{
		return cudao.crearCuenta(cuenta);
	}
	
	public List<Cliente> listarClientes() throws Exception{
		return cdao.listarClientes();
	}
	
	public List<Cuenta> listarCuentas() throws Exception{
		return cudao.listarCuentas();
	}
	
	public Cliente buscarCliente(String cedula) throws Exception{
		return cdao.buscarCliente(cedula);
	}
}
