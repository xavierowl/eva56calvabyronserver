package ec.edu.ups.on;

import java.util.List;

import javax.ejb.Local;

import ec.edu.ups.modelos.Cliente;
import ec.edu.ups.modelos.Cuenta;

@Local
public interface GestionBancoLocal {
	public boolean crearCliente(Cliente cliente) throws Exception;
	public boolean crearCuenta(Cuenta cuenta) throws Exception;
	public List<Cliente> listarClientes() throws Exception;
	public List<Cuenta> listarCuentas() throws Exception;
	public Cliente buscarCliente(String cedula) throws Exception;
}
